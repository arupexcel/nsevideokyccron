﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSEVideoKYCCron
{
    public partial class VideoKYCStatusPull : Form
    {
        SqlConnection ConPortView = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        SqlConnection Con = new SqlConnection();
        SqlCommand cmd;
        string xStr = "";
        string xBid = "";
        string xCid = "";

        public VideoKYCStatusPull()
        {
            InitializeComponent();
        }

        private void VideoKYCStatusPull_Load(object sender, EventArgs e)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            xStr = "select distinct dbname, bid from dbsetting ";
            DataSet dsParam = new DataSet();

            SqlCommand sqlparamcmd = new SqlCommand(xStr, ConPortView);
            sqlparamcmd.CommandTimeout = 0;
            SqlDataAdapter daParam = new SqlDataAdapter(sqlparamcmd);
            ConPortView.Open();
            daParam.Fill(dsParam);
            ConPortView.Close();

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("dbname", typeof(string)));
            dt.Columns.Add(new DataColumn("Bid", typeof(string)));
            dt.Columns.Add(new DataColumn("Cid", typeof(string)));
            dt.Columns.Add(new DataColumn("Email", typeof(string)));
            dt.Columns.Add(new DataColumn("Username", typeof(string)));
            dt.Columns.Add(new DataColumn("merchantId", typeof(string)));
            dt.Columns.Add(new DataColumn("CustomerID", typeof(string)));
            dt.Columns.Add(new DataColumn("KYCStatus", typeof(string)));
            dt.Columns.Add(new DataColumn("PANNo", typeof(string)));
            dt.Columns.Add(new DataColumn("SQL", typeof(string)));

            DataRow row;
            try
            {
                foreach (DataRow dr in dsParam.Tables[0].Rows)
                {
                    xStr = "select Bid,Cid,Email,Username,merchantId=objectiveID,CustomerID,KYCStatus,PANNo from " + dr["dbname"].ToString() + ".dbo.VideoKYCDetail with (nolock) where isnull(KYCStatus,'') in ('DRAFT','PENDING','','INITIATED','No')";

                    DataSet ds = new DataSet();

                    sqlparamcmd = new SqlCommand(xStr, ConPortView);
                    sqlparamcmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(sqlparamcmd);
                    ConPortView.Open();
                    da.Fill(ds);
                    ConPortView.Close();
                    foreach (DataRow vrow in ds.Tables[0].Rows)
                    {
                        row = dt.NewRow();
                        row["dbname"] = dr["dbname"].ToString();
                        row["Bid"] = vrow["Bid"].ToString();
                        row["Cid"] = vrow["Cid"].ToString();
                        row["Email"] = vrow["Email"].ToString();
                        row["Username"] = vrow["Username"].ToString();
                        row["merchantId"] = vrow["merchantId"].ToString();
                        row["CustomerID"] = vrow["CustomerID"].ToString();
                        row["KYCStatus"] = vrow["KYCStatus"].ToString();
                        row["PANNo"] = vrow["PANNo"].ToString();
                        dt.Rows.Add(row);

                    }

                    //Getting KYC status from NSE   
                    
                    xStr = "select NSEUserName,NSEMemberID,NSEPassword,NSEEUIN,subdomain from " + dr["dbname"].ToString() + ".dbo.subdomaininfo with (nolock) where bid='" + dr["Bid"].ToString() + "' ";
                    SqlDataAdapter daNSECred = new SqlDataAdapter(xStr, ConPortView);
                    DataSet dsNSECred = new DataSet();

                    ConPortView.Open();
                    daNSECred.Fill(dsNSECred);
                    ConPortView.Close();

                    string xUserid = dsNSECred.Tables[0].Rows[0]["NSEUserName"].ToString();
                    string xArnNo = dsNSECred.Tables[0].Rows[0]["NSEMemberID"].ToString();
                    string xPassword = dsNSECred.Tables[0].Rows[0]["NSEPassword"].ToString();
                    string xEUIN = dsNSECred.Tables[0].Rows[0]["NSEEUIN"].ToString();

                    string toDate = DateTime.Today.ToString("dd-MMM-yyyy");
                    string fromDate = DateTime.Today.AddMonths(-2).ToString("dd-MMM-yyyy");

                    StringBuilder IINTransxStringBuild = new StringBuilder();
                    IINTransxStringBuild.Append("<?xml version='1.0' encoding='UTF-8'?>");
                    IINTransxStringBuild.Append("<NMFIIService xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");
                    IINTransxStringBuild.Append("<service_request>");
                    IINTransxStringBuild.AppendFormat("<appln_id>{0}</appln_id>", xUserid);
                    IINTransxStringBuild.AppendFormat("<broker_code>{0}</broker_code>", xArnNo);
                    IINTransxStringBuild.AppendFormat("<password>{0}</password>", xPassword);
                    IINTransxStringBuild.AppendFormat("<pan>{0}</pan>", "");
                    IINTransxStringBuild.AppendFormat("<from_date>{0}</from_date>", fromDate);
                    IINTransxStringBuild.AppendFormat("<to_date>{0}</to_date>", toDate);
                    IINTransxStringBuild.Append("</service_request>");
                    IINTransxStringBuild.Append("</NMFIIService>");

                    System.Xml.XmlDocument xrecord = new System.Xml.XmlDocument();
                    xrecord.LoadXml(IINTransxStringBuild.ToString());

                    WebRequest req = WebRequest.Create(@"https://www.nsenmf.com/NMFIITrxnService/NMFTrxnService/eKYC_STATUS_REPORT");

                    req.Method = "POST";
                    req.ContentType = "application/xml; charset=utf-8";
                    string sXml = xrecord.InnerXml;
                    req.ContentLength = sXml.Length;
                    StreamWriter sw = new StreamWriter(req.GetRequestStream());
                    sw.Write(sXml);
                    sw.Close();
                    WebResponse res = req.GetResponse();
                    DataSet dsIINDet = new DataSet();
                    StreamReader stream = new StreamReader(res.GetResponseStream());
                    //string sXML = stream.ReadToEnd();                
                    dsIINDet.ReadXml(stream);
                    DataTable dt1 = dsIINDet.Tables[0];
                    DataTable dt2 = dsIINDet.Tables[1];

                    if (dsIINDet.Tables["service_status"].Rows[0]["service_return_code"].ToString() == "0")
                    {
                        foreach (DataRow dr1 in dt2.Rows)
                        {
                            string xPan = dr1["PAN"].ToString();
                            string KYCStatus = dr1["VERIFY_STATUS"].ToString();

                            if (KYCStatus == "No") { KYCStatus = "Initiated"; }
                            else if (KYCStatus == "Yes") { KYCStatus = "accepted"; }

                            foreach (DataRow dbKyc in dt.Rows)
                            {
                                if (xPan == dbKyc["PANNo"].ToString())
                                {
                                    xStr = " Update " + dbKyc["dbname"].ToString() + ".dbo.VideoKYCDetail set StatusDate=getDate(), KYCStatus='" + KYCStatus + "',PANNo='" + xPan + "' where bid='" + dbKyc["bid"].ToString() + "' and PANNo='" + xPan + "' ";

                                    cmd = new SqlCommand(xStr, ConPortView);
                                    ConPortView.Open();
                                    cmd.ExecuteNonQuery();
                                    ConPortView.Close();
                                }
                               
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConPortView.Close();
                if (ConPortView.State == ConnectionState.Open) ConPortView.Close();
                xStr = "insert into VideoKYCCronstats (cronDate,NoOfKYC,CronStatus) values (getdate(),0,'Error')";
                cmd = new SqlCommand(xStr, ConPortView);
                ConPortView.Open();
                cmd.ExecuteNonQuery();
                ConPortView.Close();
            }



           
        }
    }
}
